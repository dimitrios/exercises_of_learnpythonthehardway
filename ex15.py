# Imports module argv as I saw from console it is an array
from sys import argv

script, filename = argv
# Must be opening the file for reading:
txt = open(filename)

print "Here's your file %r:" % filename
print txt.read() # Now reads the file into a string, what if file is too big???
txt.close()

# Ditto:
print "Type the filename again:"
file_again = raw_input("> ")

txt_again = open(filename)

print txt_again.read()
txt_again.close()

