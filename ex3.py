print "I will note count my chickets:" # prints some text

print "Hens", 25 + 30 / 6 # Prints hens and then calculation w. default precedence
print "Roosters", 100 - 25 * 3 % 4 # Same as above with a modulo

print "Now I will count the eggs:"

print 3 + 2 + 1 + -5 + 4 % 2 - 1 / 4 + 6 # Long calculation w. default precedence, for me the output is unknown

print "Is it true that 3 + 2 < 5 - 7?" # 

print 3 + 2 < 5 - 7 # Prints truw or false?

print "What is 3 + 2?", 3 + 2
print "What is 5 - 7?", 5 - 7

print "Oh, that's why it's False." # So + and - are calcuated before <

print "How about some more." 

# Some comparissons printed out:
print "Is this greater?", 5 > -2
print "Is this greater or equal?", 5 >= -2
print "Is this less or equal", 5 <= -2
