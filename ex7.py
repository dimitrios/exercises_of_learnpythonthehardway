print "Mary had a little lamb." # Prints the string
print "Its fleece was white as %s." % 'snow' # Prints string after resolfing formatting
print "And everywhere that Mary went." # Prints string
print "." * 10 # would print ten dots? Multiplies string integer times

end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that comma at the end. try removing it to see what happens
print end1 + end2 + end3 + end4 + end5 + end6, # Seems to print string concatenation and then not pring a new line waiting for next print statement.
print end7 + end8 + end9 + end10 + end11 + end12 # Continues from previous line printing string concatenation.
