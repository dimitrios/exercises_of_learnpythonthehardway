def get_numbers_list(num, incr=1):
    i = 0
    numbers = []

    while i < num:
        print "At the top i is %d" % i
        numbers.append(i)

        i = i + incr
        print "Numbers now:", numbers
        print "At the bottom i is %d" % i
    
    return numbers

print "The numbers: "

for num in get_numbers_list(9, 2):
    print num

